<?php

use Illuminate\Database\Seeder;

class CategoiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoies = array (
        	array(
        		'category_name'=>'Category 1',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
        		'category_name'=>'Category 2',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
        		'category_name'=>'Category 3',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
        		'category_name'=>'Category 4',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	),
        	array(
        		'category_name'=>'Category 5',
        		'created_at'=>\Carbon\Carbon::now(),
        		'updated_at'=>\Carbon\Carbon::now(),
        	)
		);
		$db = DB::table('categories')->insert($categoies);
    }
}
