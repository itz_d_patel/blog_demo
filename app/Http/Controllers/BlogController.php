<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.blog.list')->with('headTitle','Blogs');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('pages.blog.add')->with('categories',$categories)->with('headTitle','Add Blogs');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'category_id' => 'required',
            'blog_name' => 'required',
            'blog_description' => 'required'
        ];
        $this->validateForm($request->all(), $rules);

        $blog_data = [
            'blog_name' => $request->blog_name,
            'blog_description' => $request->blog_description,
            'blog_date' => \Carbon\Carbon::now()->format('Y-m-d'),
        ];

        $blog = Blog::create($blog_data);
        $blog->category()->sync($request->category_id);
        return redirect()->route('blog.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
    }

    public function listdata(Request $request)
    {
        extract($this->DTFilters($request->all()));
        $count = Blog::where('id','>', 0);
        if($search != ''){
            $count->where(function($query) use ($search){
                $query->orwhere("blog_name", "like", "%{$search}%");
            });
        }
        if ($request['status']) {
            $count = $count->where('blog_date',$request['status']);
        }
        $count = $count->count();

        $records["recordsTotal"] = $count;
        $records["recordsFiltered"] = $count;
        $records['data'] = array();
        
        $blogs = Blog::where('id','>', 0)->offset($offset)->limit($limit)->orderBy($sort_column,$sort_order);
        if($search != ''){
            $blogs->where(function($query) use ($search){
                $query->orwhere("blog_name", "like", "%{$search}%");
            });
        }
        if ($request['status']) {
            $blogs = $blogs->where('blog_date',$request['status']);
        }
        $blogs = $blogs->get();

        foreach ($blogs as $blog) {
            $records['data'][] = [
                'blog_name'=>$blog->blog_name,
                'blog_description'=>$blog->blog_description,
                'blog_date'=>\Carbon\Carbon::parse($blog->created_at)->format('Y-m-d'),
                // 'action'=>'<a href='.url('blog/' . $blog->id).'><button name="view" id="view" class="btn btn-primary btn-xs btnView">View</button></a> <a href='.route('blog.destroy' , $blog->id).' class="btnDelete"><button name="delete" id="delete" class="btn btn-danger btn-xs">Delete</button></a> <a href='.url("blog/".$blog->id."/edit").'><button name="edit" id="edit" class="btn btn-warning btn-xs">Edit</button></a>'
            ];
        }
        return $records;
    }
}
