@extends('layouts.app')
@section('title') {{ $headTitle }} @endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Insert Blog</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" id="frmAdd" action="{{ route('blog.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                            <label for="blog_name" class="col-md-4 control-label">Category</label>

                            <div class="col-md-6">
                                <select data-error-container="#category_error" name="category_id[]" id="category_id" class="form-control input-sm select2-multiple" multiple="multiple">
                                    <option value="" disabled>Select Category</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}" {{ (old('category_id') == $category->id) ? 'selected' : '' }}>{{$category->category_name}}</option>
                                    @endforeach
                                </select>
                                <span id="category_error" class="has-error"></span>

                                @if ($errors->has('category_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('blog_name') ? ' has-error' : '' }}">
                            <label for="blog_name" class="col-md-4 control-label">Blog Name</label>

                            <div class="col-md-6">
                                <input id="blog_name" type="text" class="form-control" name="blog_name">

                                @if ($errors->has('blog_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('blog_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('blog_description') ? ' has-error' : '' }}">
                            <label for="blog_description" class="col-md-4 control-label">Blog Description</label>

                            <div class="col-md-6">
                                <textarea id="blog_description" rows="5" class="form-control" name="blog_description"></textarea>

                                @if ($errors->has('blog_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('blog_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" id="btn-insert" value="insert">Submit</button>
                                <a href="{{ route('blog.index') }}"> <button type="button" class="btn btn-warning" id="btn-insert" value="insert">Back</button></a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
    
    $("#category_id").select2({placeholder: "Select Category"});

    $(document).on('submit','#frmAdd', function(e){
        $("#frmAdd").validate({
            rules: {
                "category_id[]":{required:true},
                blog_name:{required:true},
                blog_description:{required:true}
            },
            messages: {
                category_id:{required:"Please enter category."},
                blog_name:{required:"Please enter blog name.",},
                blog_description:{required:"Please enter blog description.",}
            },
            errorClass: 'help-block',
            errorElement: 'span',
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorPlacement: function (error, element) {
                if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else {
                    error.insertAfter(element);
                }
            }
        });
    });
    $(document).on('submit','#frmAdd', function(e) {
        if ($("#frmAdd").valid()) {
            return true;
        } else {
            return false;
        }
    });
});
</script>
@endpush


