@extends('layouts.app')
@section('title') {{ $headTitle }} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('edit_store', $store) !!}
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-form">{{ $headTitle }}</h4>
                <a class="heading-elements-toggle">
                    <i class="la la-ellipsis-v font-medium-3"></i>
                </a>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <form class="form-horizontal" role="form" id="frmEditStore" method="POST" action="{{ route('store.update', $store->id) }}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}

                                <div class="form-group{{ $errors->has('store_name') ? ' has-error' : '' }}">
                                    <label class="col-md-6">{!! $mend_sign !!}Store Name</label>
                                    <div class="col-md-12 col-lg-12 col-xl-6">
                                        <div class="position-relative has-icon-left">
                                            <input type="text" placeholder="Enter Store Name" name="store_name" id="store_name" class="form-control" value="{{old('store_name', $store->store_name)}}" />
                                            <div class="form-control-position">
                                                <i class="fa fa-font"></i>
                                            </div>
                                            @if ($errors->has('store_name'))
                                                <span class="danger"><strong>{{ $errors->first('store_name') }}</strong></span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('store_location') ? ' has-error' : '' }}">
                                    <label class="col-md-3 control-label">{!! $mend_sign !!}Store Location</label>
                                    <div class="col-md-12 col-lg-12 col-xl-6">
                                        <div class="position-relative has-icon-left">
                                            <input type="text" placeholder="Enter Store Location" name="store_location" id="store_location" class="form-control" value="{{old('store_location', $store->store_location)}}" data-error-container="#store_location_error"/>
                                            <div class="form-control-position">
                                                <i class="fa fa-map"></i>
                                            </div>
                                            <div id="map" style="width:100%;height:400px"></div>
                                            <span id="store_location_error"></span>
                                        </div>
                                    </div>
                                </div>


                                
                                <div class="margiv-top-10">
                                	<a href="{{ route('store.index') }}"><button type="button" class="btn btn-danger mr-1"><i class="fa fa-arrow-left"></i> Back</button></a>
                                    <button type="submit" class="btn btn-primary btn-glow"><i class="fa fa-check"></i> Save Changes </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function() {
	$("#frmEditStore").validate({
        rules: {
            store_name:{
                required:true
            },
            store_location:{
                required:true
            }
        },
        messages: {
            store_name:{
                required:"@lang('validation.required',['attribute'=>'store name'])"
            },
            store_location:{
                required:"@lang('validation.required',['attribute'=>'store location'])"
            }
        },
        errorClass: 'danger',
        errorElement: 'span',
        highlight: function (element) {
           $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
           $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.attr("data-error-container")) {
                error.appendTo(element.attr("data-error-container"));
            } else {
                error.insertAfter(element);
            }
        }
    });
    $(document).on('submit','#frmEditStore',function(){
        if($("#frmEditStore").valid()) {
            return true;
        }else {
            return false;
        }
    });
});

//GOOGLE MAP
function initAutocomplete() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat:{{ ($store->store_latitude != 0) ? $store->store_latitude : 42.7288916 }}, lng: {{ ($store->store_longitude != 0) ? $store->store_longitude : -87.7847695 }} },
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false
    });

    var marker = new google.maps.Marker({map: map});
    var geocoder = new google.maps.Geocoder();
  
    // Create the search box and link it to the UI element.
    var input = document.getElementById('store_location');
    var searchBox = new google.maps.places.SearchBox(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });

    var markers = [];

    @if ($store->store_location != "")
        markers.push(new google.maps.Marker({
            map: map,
            position: {lat:{{ $store->store_latitude }}, lng: {{ $store->store_longitude }} }
        }));
    @endif

    // Listen for the event fired when the user selects a prediction and retrieve more details for that place.
    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();
        if (places.length == 0) {
            return;
        }
        // Clear out the old markers.
        markers.forEach(function(marker) {
            marker.setMap(null);
        });
        markers = [];
        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            // Create a marker for each place.
            markers.push(new google.maps.Marker({
                map: map,
                title: place.name,
                position: place.geometry.location
            }));

            if (place.geometry) {
                marker.setPosition(place.geometry.location);
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            }
        });
        map.fitBounds(bounds);
    });
}
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC7kiVlXV3vw-HjT_9UmKEoJ5su1KXJrBA&libraries=places&callback=initAutocomplete" type="text/javascript">
</script>
</script>
@endpush