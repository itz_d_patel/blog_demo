@extends('layouts.app')
@section('title') {{ $headTitle }} @endsection

@section('breadcrumb')
{!! Breadcrumbs::render('view_store', $store) !!}
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title" id="basic-layout-form">{{ $headTitle }}</h4>
                <a class="heading-elements-toggle">
                    <i class="la la-ellipsis-v font-medium-3"></i>
                </a>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card-content collapse show">
                        <div class="card-body">
                            <dl class="row">
                                <dt class="col-md-3 font-small-4">Store Name:</dt>
                                <dd class="col-md-4 font-small-4">{{ $store->store_name }}</dd>
                            </dl>

                            <dl class="row">
                            	<dt class="col-md-3 font-small-4">Store Description:</dt>
                                <dd class="col-md-4 font-small-4">{{ $store->store_description }}</dd>
                            </dl>

                            <dl class="row">
                                <dt class="col-md-3 font-small-4">Store Location:</dt>
                                <dd class="col-md-4 font-small-4">{{ $store->store_location }}</dd>
                            </dl>

                            <dl class="row">
                                <dt class="col-md-3 font-small-4">Store URL:</dt>
                                <dd class="col-md-4 font-small-4">{{ $store->url }}</dd>
                            </dl>

                            <dl class="row">
	                        	<dt class="col-md-3 font-small-4">Store Logo:</dt>
                                <dd class="col-md-4">
	                               <img width="200px" height="150px" src="{{ checkImage(7,$store->store_logo) }}" alt="" class="img-responsive" id="store_logo" name="store_logo">
                                </dd>
		                    </dl>

                            <dl class="row">
                                <dt class="col-md-3 font-small-4">Store Banner:</dt>
                                <dd class="col-md-4">
                                   <img width="200px" height="150px" src="{{ checkImage(7,$store->store_banner) }}" alt="" class="img-responsive" id="store_banner" name="store_banner">
                                </dd>
                            </dl>
                            
                            <div class="margiv-top-10">
                                <a href="{{ route('store.index') }}"><button type="button" class="btn btn-danger mr-1"><i class="fa fa-arrow-left"></i> Back</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection