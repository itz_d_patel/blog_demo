@extends('layouts.app')
@section('title') {{ $headTitle }} @endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ url('blog/create') }}"><button name="add" id="btn-add" class="btn btn-success btn-sm pull-right" style="margin-top: -5px;">Add</button></a>
                </div>
                <div class="row">
                    <label for="filter" class="col-md-2 control-label">Date Filter</label>
                    <div class='col-md-4 input-group date' id='filter_date'>
                        <input type='text' class="form-control" id="datevalue" readonly="" />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="tbl_blog">
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">

function tableData(val = ''){
    console.log(val);
    oTable = $('#tbl_blog').DataTable({
        "processing": true,
        "serverSide": true,
        "language": {
            "aria": {
                "sortAscending": ": click to sort column ascending",
                "sortDescending": ": click to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },
        "columns": [
            { "title": "Blog Name" ,"data": "blog_name"},
            { "title": "Blog Description" ,"data": "blog_description"},
            { "title": "Blog Date" ,"data": "blog_date"},
            // { "title": "Action" ,"data": "action", searchble: false, sortable:false }
        ],
        "order": [
            [0, 'asc']
        ],
        "lengthMenu": [
            [10, 20, 50,100],
            [10, 20, 50,100]
        ],
        "ajax": {
            "url": "{{ route('blog.listdata') }}",
            "data": {status:val},
        },
    });
};

$(function(){
    tableData();

    $('#filter_date').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
    });

    $('#filter_date').change(function() {
        var status = $("#datevalue").val();
        $('#tbl_blog').dataTable().fnDestroy();
        tableData(status);
    });
});

</script>
@endpush
